package com.xm.data.main;

import org.junit.Test;

public class Main {

	private final static String JS = "phantomjs";
	private final static String SCRIPT = "html.js";

	@Test
	public void testJS() {
		String url = "https://www.zhihu.com/org/fa-xian-lu-xing-89/activities";
		String command = JS + " " + SCRIPT + " " + url;

		LocalCommandExecutor commandExecutor = new LocalCommandExecutorImpl();
		ExecuteResult result = commandExecutor.executeCommand(command, 15000);
		System.out.println(result);
	}
}
