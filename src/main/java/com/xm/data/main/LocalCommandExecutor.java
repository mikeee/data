package com.xm.data.main;

public interface LocalCommandExecutor {
	ExecuteResult executeCommand(String command, long timeout);
}
