var RenderUrlsToFile, arrayOfUrls, system;

var fs = require('fs');
system = require("system");

RenderUrlsToFile = function(urls, callbackPerUrl, callbackFinal) {
	var getFilename, next, page, retrieve, urlIndex, webpage;
	urlIndex = 0;
	webpage = require("webpage");
	page = null;
	getFilename = function() {
		return "data/content.png";
	};
	next = function(status, url, file) {
		page.close();
		callbackPerUrl(status, url, file);
		return retrieve();
	};
	retrieve = function() {
		var url;
		if (urls.length > 0) {
			url = urls.shift();
			urlIndex++;
			page = webpage.create();
			page.viewportSize = {
				width : 800,
				height : 600
			};
			page.settings.userAgent = "Phantom.js bot";
			return page.open(url, function(status) {
				var file;
				file = getFilename();
				if (status === "success") {
					return window.setTimeout(function() {
						page.render(file);
						var content = page.content;
						fs.write('data/content.html', content, 'w');
						return next(status, url, file);
					}, 200);
				} else {
					return next(status, url, file);
				}
			});
		} else {
			return callbackFinal();
		}
	};
	return retrieve();
};

arrayOfUrls = null;

if (system.args.length > 1) {
	arrayOfUrls = Array.prototype.slice.call(system.args, 1);
}

RenderUrlsToFile(arrayOfUrls, function(status, url, file) {
	if (status !== "success") {
		return console.log("Unable to render '" + url + "'");
	} else {
		return console.log("Rendered '" + url + "' at '" + file + "'");
	}
}, function() {
	return phantom.exit();
});